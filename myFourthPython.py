prog_values = ('Java', 'JavaScript', 'Python')

print("Value at first index is "+prog_values[0])
print("Total element in tuples is ", len(prog_values))
print("Count of Java ", prog_values.count('Java'))

print(prog_values)

lis = list(prog_values)
lis.append('Scala')
prog_values = tuple(lis)
print(prog_values)

def sum(a = 30, b = 90, c = 100, d = 100):
    # a = int(input("Enter Principal Amount"))
    # b = int(input("Enter Principal Amount"))
    # c = int(input("Enter Principal Amount"))
    #a = int(input("Enter Principal Amount"))
    return a + b + c + d

print('The sum is ', sum(10, 20))